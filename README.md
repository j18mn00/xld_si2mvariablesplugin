# SI2M Custom Variables Plugin #

XLDeploy SI2M custom plugin for Variables containers.

### BUILD STATUS ###



* * *

### PREREQUISITE ###

No prerequisite

### INSTALLATION ###

Download the last jar and install it in `<XLDEPLOY_SERVER_HOME>/plugins` directory.
 
Restart XLDeploy server to take account of the additionals properties.

### USAGE ###

Specific SI2M properties for variables .exxx file in /appli/script.

* * *
### UPDATES ###

**7.0-02**   :  Update Version

**7.0-01**   :  Initial Version



* * *
### SPECIFICS MODIFICATIONS ###

#### Types Modifications ####

N/A

#### New Types Added ####

These types have been added to respond to specific SI2M needs.

* `si2m.variablesContainer` : Container wich will receive variables files.


Admin types :

* `si2m.variablesDeployedApp` : Specific type to deploy artefacts with permissions.



Useful types :

* `si2m.Variables` : Specific type for variables configuration files.



* * *

### MAINTAINERS ###

* SI2M - ISOD TEAM : G. LANTERI