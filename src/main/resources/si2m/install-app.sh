echo "#############################"
#user=`whoami`
echo Installing archive ${deployed.deployable.file} in ${deployed.targetPath}


#echo sudoUsername : ${deployed.container.host.sudoUsername}
<#if deployed.targetFileName?has_content>
TARGET_FILE_NAME=${deployed.targetFileName}
<#else>
TARGET_FILE_NAME=${deployed.name}
</#if>

TARGET_FILE=${deployed.targetPath}/$TARGET_FILE_NAME
echo Creating "$TARGET_FILE"
ls -trl "${deployed.file}"


echo copy targetFile :
ls -trl  $TARGET_FILE
cp -p "${deployed.file}" "$TARGET_FILE"
res=$?
if [ $res != 0 ] ; then
  exit $res
else
        ls -trl  $TARGET_FILE
        echo "[OK]"
fi

echo "change user :"
chown $USER:$USER $TARGET_FILE
res=$?
if [ $res != 0 ] ; then
  exit $res
else
        ls -trl  $TARGET_FILE
        echo "[OK]"
fi

echo "set rights 640 :"
chmod 640 $TARGET_FILE
res=$?
if [ $res != 0 ] ; then
  exit $res
else
        ls -trl  $TARGET_FILE
        echo "[OK]"
fi
echo "#############################"
