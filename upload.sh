#!/bin/bash
files=$(shopt -s nullglob dotglob; echo target/*jar) > /dev/null 2>&1
echo $files
if (( ${#files}  ))
then 
        for file in $files 
            do
                    echo "Filename"
                    echo $file
                    curl -X POST --user "${BB_AUTH_STRING}" "https://api.bitbucket.org/2.0/repositories/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/downloads" --form "files=@$file"
            done;
fi 